import cherrypy
import docker
import os
import re
import requests

client = docker.from_env()

prometheus_ports = {
  'server': os.getenv('PROMETHEUS_PORT_SERVER', '9190'),
  'subnode': os.getenv('PROMETHEUS_PORT_SUBNODE', '9190'),
}

class MetricFetcher(object):
  @cherrypy.expose
  def metrics(self):
    lines = []

    for container in client.containers.list():
      try:
        container_name = container.attrs['Name'][1:]
        container_image = container.attrs['Config']['Image']

        m = re.match('^registry\\.gitlab\\.com/chromaway/postchain-chromia/chromaway/chromia-(server|subnode):.*$', container_image)

        if m is None:
          continue

        postchain_type = m.group(1)

        ip_address = container.attrs['NetworkSettings']['IPAddress']
        r = requests.get(
          'http://' + ip_address + ':' + prometheus_ports[postchain_type] + '/metrics',
          timeout=5,
        ).text

        for line in r.split('\n'):
          if not line.lstrip().startswith('#'):
            line = line.replace('{', '{container="' + container_name + '",postchain_type="' + postchain_type + '",', 1)

          lines.append(line)

      except Exception as e:
        print('failed to retrieve data from a container:')
        print(e)

    return '\n'.join(lines)

def error_page_404(status, message, traceback, version):
  return ''

cherrypy.config.update({
  'log.screen': False,
  'log.access_file': '',
  'log.error_file': '',
  'server.socket_host': '0.0.0.0',
  'error_page.404': error_page_404,
})

if __name__ == '__main__':
  cherrypy.quickstart(MetricFetcher(), '/')
