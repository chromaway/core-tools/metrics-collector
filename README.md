# metrics-collector
This folder holds the source code for a utility application that, via Docker, collects Prometheus metrics from Docker containers running Postchain. The utility iterates through the list of Docker containers on the host system and queries the containers that run Postchain, determined by that they run the `chromia-server` or `chromia-subnode` Docker image. The metrics collected are tagged with the respective image's name, and then all metrics are concatenated and returned.

## Configuration
By passing the enviroment variables `PROMETHEUS_PORT_SERVER` or `PROMETHEUS_PORT_SUBNODE`, it's possible to let this utility query non-standard Prometheus ports for the master node and the subnodes, respectively. See `metrics.prometheus.port` and `container.metrics.prometheus.port` in the node configuration file (`[...].properties`) to learn which ports that are used.

| Node configuration property         | Environment variable for metrics-collector   | Default value |
|-------------------------------------|----------------------------------------------|---------------|
| `metrics.prometheus.port`           | `PROMETHEUS_PORT_SERVER`                     | 9190          |
| `container.metrics.prometheus.port` | `PROMETHEUS_PORT_SUBNODE`                    | 9190          |

## Usage
To run this utility, Docker can be used, on the same host system that runs Postchain:
```
$ docker run \
     --detach \
     --name metrics-collector \
     --restart unless-stopped \
     --volume /var/run/docker.sock:/var/run/docker.sock \
     --publish 8080:8080/tcp \
     --env PROMETHEUS_PORT_SERVER=9190 \
     --env PROMETHEUS_PORT_SUBNODE=9190 \
     registry.gitlab.com/chromaway/core-tools/metrics-collector:latest
```

When the container is running, you can query it with an HTTP client:
```
$ curl 'http://localhost:8080/metrics'
```
