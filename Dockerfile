FROM python:3.12-bookworm

ADD requirements.txt /requirements.txt
ADD main.py /main.py

RUN pip3 install -r /requirements.txt

CMD ["/main.py"]
ENTRYPOINT ["python"]
